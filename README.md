# Git visualization for designers

Slides from the talk given at [Libre Graphics Meeting 2016](http://libregraphicsmeeting.org) in London.

### How to use & modify

`git clone --recursive https://gitlab.com/xuv/git-visualization-for-designers.git`
